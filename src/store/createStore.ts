import { AuthState, AuthAction } from './modules/auth/types';
import { createStore, applyMiddleware, Reducer, Middleware } from 'redux';
import { EnterprisesState } from './modules/enterprises/types';

export interface IApplicationState {
  auth: AuthState;
  enterprises: EnterprisesState;
}

export type ApplicationActions = AuthAction;

export default (
  reducers: Reducer<IApplicationState, ApplicationActions>,
  middlewares: Middleware[],
) => {
  const enhancer = applyMiddleware(...middlewares);

  return createStore(reducers, enhancer);
};
