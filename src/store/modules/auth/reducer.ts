import { AuthState, AuthAction } from './types';

const initialState: AuthState = {
  loading: false,
  isAppLoading: null,
  isSigned: null,
  user: null,
};

export default function auth(state = initialState, action: AuthAction): AuthState {
  switch (action.type) {
    case '@auth/LOAD_APP':
      return { ...state, isAppLoading: true };
    case '@auth/SET_APP_DATA':
      return {
        ...state,
        isAppLoading: false,
        isSigned: action.payload.hasToken,
        user: action.payload.user,
      };
    case '@auth/SIGN_IN_REQUEST':
      return {
        ...state,
        loading: true,
      };
    case '@auth/SIGN_IN_SUCCESS':
      return {
        ...state,
        loading: false,
        isSigned: true,
        user: action.payload.payload,
      };
    case '@auth/SIGN_IN_FAILURE':
      return {
        ...state,
        loading: false,
      };
    case '@auth/SIGN_OUT':
      return { ...state, isSigned: false };
    default:
      return state;
  }
}
