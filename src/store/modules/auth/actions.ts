import { action } from 'typesafe-actions';
import { IAppData, ISignInAction, IUserSigned } from './types';

export function loadApp() {
  return action('@auth/LOAD_APP');
}

export function setAppData(payload: IAppData) {
  return action('@auth/SET_APP_DATA', payload);
}

export function signInRequest(payload: ISignInAction) {
  return action('@auth/SIGN_IN_REQUEST', payload);
}

export function signInSuccess(payload: IUserSigned) {
  return action('@auth/SIGN_IN_SUCCESS', {
    payload,
  });
}

export function signInFailure() {
  return action('@auth/SIGN_IN_FAILURE');
}

export function signOut() {
  return action('@auth/SIGN_OUT');
}
