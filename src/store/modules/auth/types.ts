import { ActionType } from 'typesafe-actions';
import * as actions from './actions';

export type AuthAction = ActionType<typeof actions>;

export interface AuthState {
  readonly loading: Boolean;
  readonly isAppLoading: Boolean | null;
  readonly isSigned: Boolean | null;
  readonly user: IUserSigned | null;
}

export interface ISignInAction {
  email: string;
  password: string;
}

export interface IUserSigned {
  name: string;
  email: string;
}

export interface IAppData {
  hasToken: boolean;
  user: IUserSigned | null;
}
