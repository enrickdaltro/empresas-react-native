import { Alert } from 'react-native';
import { takeLatest, call, put, all } from 'redux-saga/effects';
import { ActionType } from 'typesafe-actions';
import api from '../../../services/api';
import {
  saveString,
  hasToken,
  loadString,
  save,
  hasHeaders,
  load,
  remove,
  appUser,
} from '../../../services/storage';
import * as actions from './actions';
import * as RootNavigation from '../../../services/root-navigation';

export function* loadApp() {
  const hasAuthToken = yield loadString(hasToken);
  const hasApiHeaders = yield load(hasHeaders);
  const hasUser = yield load(appUser);

  if (hasAuthToken) {
    api.defaults.headers['access-token'] = hasApiHeaders.token;
    api.defaults.headers.uid = hasApiHeaders.uid;
    api.defaults.headers.client = hasApiHeaders.client;

    // check if token is still valid
    const { status } = yield call(api.get, '/enterprises');

    if (status === 200) {
      // if valid, continue
      yield put(actions.setAppData({ hasToken: true, user: hasUser }));
    } else {
      // if not, sign out the user
      signOut();
    }
  } else {
    yield put(actions.setAppData({ hasToken: false, user: null }));
  }
}

export function* signIn(action: ActionType<typeof actions.signInRequest>) {
  try {
    // call api
    const { data, headers } = yield call(api.post, '/users/auth/sign_in', action.payload);

    // set custom headers
    api.defaults.headers['access-token'] = headers['access-token'];
    api.defaults.headers.uid = headers['uid'];
    api.defaults.headers.client = headers['client'];

    // call action
    const user = {
      name: data.investor.investor_name,
      email: data.investor.email,
    };
    yield put(actions.signInSuccess(user));

    // store token and headers
    const apiHeaders = {
      token: headers['access-token'],
      uid: headers['uid'],
      client: headers['client'],
    };
    yield call(saveString, hasToken, headers['access-token']);
    yield call(save, hasHeaders, apiHeaders);
    yield call(save, appUser, user);

    // navigate
    RootNavigation.navigate('Home');
  } catch (error) {
    // error handling
    yield put(actions.signInFailure());
    Alert.alert('Hang on!', '"Invalid login credentials. Please try again."');
  }
}

export function* signOut() {
  yield call(remove, hasToken);
  yield call(remove, hasHeaders);
  yield call(remove, appUser);
  RootNavigation.reset();
}

export default all([
  takeLatest('@auth/LOAD_APP', loadApp),
  takeLatest('@auth/SIGN_IN_REQUEST', signIn),
  takeLatest('@auth/SIGN_OUT', signOut),
]);
