import auth from './auth/sagas';
import enterprises from './enterprises/sagas';
import { all } from 'redux-saga/effects';

export default function* rootSaga() {
  return yield all([auth, enterprises]);
}
