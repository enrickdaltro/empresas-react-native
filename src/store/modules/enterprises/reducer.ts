import { EnterprisesState, EnterprisesAction } from './types';

const initialState: EnterprisesState = {
  loading: false,
  filtering: false,
  loadingDetails: false,
  enterprises: null,
  enterprise: null,
  filters: null,
};

export default (state = initialState, action: EnterprisesAction): EnterprisesState => {
  switch (action.type) {
    case '@enterprises/FETCH_ENTERPRISE_REQUEST':
      return { ...state, loading: true };
    case '@enterprises/FETCH_ENTERPRISE_SUCCESS':
      return { ...state, loading: false, enterprises: action.payload };
    case '@enterprises/FETCH_ENTERPRISE_FAILURE':
      return { ...state, loading: false };
    case '@enterprises/FILTER_ENTERPRISE_REQUEST':
      return { ...state, filtering: true };
    case '@enterprises/FILTER_ENTERPRISE_SUCCESS':
      return { ...state, filtering: false, enterprises: action.payload };
    case '@enterprises/FILTER_ENTERPRISE_FAILURE':
      return { ...state, filtering: false };
    case '@enterprises/SET_FILTER_TYPES':
      return { ...state, filters: action.payload };
    case '@enterprises/FETCHING_ENTERPRISE_DETAILS_REQUEST':
      return { ...state, loadingDetails: true };
    case '@enterprises/FETCHING_ENTERPRISE_DETAILS_SUCCESS':
      return { ...state, loadingDetails: false, enterprise: action.payload };
    case '@enterprises/FETCHING_ENTERPRISE_DETAILS_FAILURE':
      return { ...state, loadingDetails: false };
    case '@enterprises/CLEAR_ENTERPRISE_DETAILS':
      return { ...state, enterprise: null };
    default:
      return state;
  }
};
