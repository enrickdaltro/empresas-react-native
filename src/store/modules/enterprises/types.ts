import { ActionType } from 'typesafe-actions';
import * as actions from './actions';

export type EnterprisesAction = ActionType<typeof actions>;

export interface EnterprisesState {
  readonly loading: Boolean;
  readonly filtering: Boolean;
  readonly loadingDetails: Boolean;
  readonly enterprises: IEnterprise[] | null;
  readonly enterprise: IEnterprise | null;
  readonly filters: IFilterType[] | null;
}

export interface IEnterpriseType {
  id: number;
  enterprise_type_name: string;
}

export interface IEnterprise {
  id: number;
  email_enterprise?: any;
  facebook?: string | null;
  twitter?: string | null;
  linkedin?: string | null;
  phone?: string | null;
  own_enterprise: boolean;
  enterprise_name: string;
  photo: string;
  description: string;
  city: string;
  country: string;
  value: number;
  share_price: number;
  enterprise_type: IEnterpriseType;
}

export interface IFilterSearch {
  name?: string;
  enterprise_types?: number | null;
}

export interface IFilterType {
  id: number;
  enterprise_type_name: string;
}
