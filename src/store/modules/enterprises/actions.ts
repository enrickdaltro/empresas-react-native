import { action } from 'typesafe-actions';
import { IEnterprise, IFilterSearch, IFilterType } from './types';

export function fetchEnterprisesRequest() {
  return action('@enterprises/FETCH_ENTERPRISE_REQUEST');
}

export function fetchEnterprisesSuccess(payload: IEnterprise[]) {
  return action('@enterprises/FETCH_ENTERPRISE_SUCCESS', payload);
}

export function fetchEnterprisesFailure() {
  return action('@enterprises/FETCH_ENTERPRISE_FAILURE');
}

export function filterEnterprisesRequest(payload: IFilterSearch) {
  return action('@enterprises/FILTER_ENTERPRISE_REQUEST', payload);
}

export function filterEnterprisesSuccess(payload: IEnterprise[]) {
  return action('@enterprises/FILTER_ENTERPRISE_SUCCESS', payload);
}

export function filterEnterprisesFailure() {
  return action('@enterprises/FILTER_ENTERPRISE_FAILURE');
}

export function setFilterTypes(filters: IFilterType[]) {
  return action('@enterprises/SET_FILTER_TYPES', filters);
}

export function fetchingEnterpriseDetailsRequest(id: number) {
  return action('@enterprises/FETCHING_ENTERPRISE_DETAILS_REQUEST', id);
}

export function fetchingEnterpriseDetailsSuccess(payload: IEnterprise) {
  return action('@enterprises/FETCHING_ENTERPRISE_DETAILS_SUCCESS', payload);
}

export function fetchingEnterpriseDetailsFailure() {
  return action('@enterprises/FETCHING_ENTERPRISE_DETAILS_FAILURE');
}

export function clearEnterpriseDetails() {
  return action('@enterprises/CLEAR_ENTERPRISE_DETAILS');
}
