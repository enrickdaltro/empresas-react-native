import { Alert } from 'react-native';
import { takeLatest, call, put, all } from 'redux-saga/effects';
import { ActionType } from 'typesafe-actions';
import api from '../../../services/api';
import { saveString, hasToken } from '../../../services/storage';
import * as actions from './actions';
import * as RootNavigation from '../../../services/root-navigation';
import { IFilterSearch, IEnterprise } from './types';
import { normalizeFilterTypes } from '../../../utils/enterprises';

export function* fetchEnterprises() {
  try {
    const { data } = yield call(api.get, '/enterprises');
    const allFiltersTypes = data.enterprises?.map((item: IEnterprise) => item.enterprise_type);
    const filterTypes = normalizeFilterTypes(allFiltersTypes);

    yield put(actions.setFilterTypes(filterTypes));
    yield put(actions.fetchEnterprisesSuccess(data.enterprises));
  } catch (error) {
    yield put(actions.fetchEnterprisesFailure());
    Alert.alert('Hang on!', 'Something went wrong, please try again.');
  }
}

export function* filterEnterprises(action: ActionType<typeof actions.filterEnterprisesRequest>) {
  try {
    const { name, enterprise_types } = action.payload;
    let params: IFilterSearch = {};
    if (name) {
      params = { name };
    }
    if (enterprise_types) {
      params = { ...params, enterprise_types };
    }
    const { data } = yield call(api.get, '/enterprises', { params });
    yield put(actions.filterEnterprisesSuccess(data.enterprises));
  } catch (error) {
    yield put(actions.filterEnterprisesFailure());
    Alert.alert('Hang on!', 'Something went wrong, please try again.');
  }
}

export function* fetchEnterpriseDetails(
  action: ActionType<typeof actions.fetchingEnterpriseDetailsRequest>,
) {
  try {
    const { data } = yield call(api.get, `/enterprises/${action.payload}`);
    yield put(actions.fetchingEnterpriseDetailsSuccess(data.enterprise));
  } catch (error) {
    yield put(actions.fetchingEnterpriseDetailsFailure());
    Alert.alert('Hang on!', 'Something went wrong, please try again.');
  }
}

export default all([
  takeLatest('@enterprises/FETCH_ENTERPRISE_REQUEST', fetchEnterprises),
  takeLatest('@enterprises/FILTER_ENTERPRISE_REQUEST', filterEnterprises),
  takeLatest('@enterprises/FETCHING_ENTERPRISE_DETAILS_REQUEST', fetchEnterpriseDetails),
]);
