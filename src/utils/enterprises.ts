import { IFilterType } from '../store/modules/enterprises/types';

export const normalizeFilterTypes = (types: IFilterType[]): IFilterType[] => {
  // let's reduce all the filters types to an array with only one of each type.
  const filteredArr = types.reduce(
    (acc, current) => {
      const type = acc.find((item) => item.id === current.id);
      return !type ? acc.concat([current]) : acc;
    },
    [{ id: 0, enterprise_type_name: 'All' }] as IFilterType[],
  );
  // sorting in alphabetical order
  const sortedFilterTypes = filteredArr.sort((a, b) =>
    a.enterprise_type_name > b.enterprise_type_name
      ? 1
      : b.enterprise_type_name > a.enterprise_type_name
      ? -1
      : 0,
  );

  return sortedFilterTypes;
};
