const images = {
  logo: require('./logo_ioasys.png'),
  leftArrow: require('./left-arrow-angle.png'),
  logoSplash: require('./logo_splash.png'),
  logout: require('./logout.png'),
};

export default images;
