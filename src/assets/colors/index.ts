const colors = {
  white: '#ffffff',
  grey: '#eeeeee',
  mediumGrey: '#cccccc',
  darkerGrey: '#666666',
  Black80: '#333333',
  black: '#000000',
};

export default colors;
