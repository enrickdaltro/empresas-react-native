import React from 'react';
import { View, Text, Image, StyleSheet, TextStyle, TouchableOpacity } from 'react-native';
import colors from '../../assets/colors';
import { IEnterprise } from '../../store/modules/enterprises/types';

interface IEnterpriseCard {
  item: IEnterprise;
  onCardPressed: (id: number) => void;
}

const EnterpriseCard = ({ item, onCardPressed }: IEnterpriseCard) => {
  const getLocalizedPrice = (price: number): string => {
    return price.toLocaleString('en-GB', { style: 'currency', currency: 'GBP' });
  };

  return (
    <TouchableOpacity style={styles.card} onPress={() => onCardPressed(item.id)}>
      <View>
        <Image
          source={{ uri: `https://empresas.ioasys.com.br${item.photo}` }}
          style={styles.image}
        />
        <View style={styles.chip}>
          <Text style={styles.chipLabel}>{item.enterprise_type.enterprise_type_name}</Text>
        </View>
      </View>
      <View style={styles.content}>
        <Text style={styles.title}>{item.enterprise_name}</Text>
        <View style={styles.contentFooter}>
          <Text style={styles.price}>{getLocalizedPrice(item.share_price)}</Text>
          <Text style={styles.price}>
            {item.city} / {item.country}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: { marginBottom: 30 },
  image: { width: '100%', height: 220, resizeMode: 'cover', borderRadius: 20 },
  chip: {
    backgroundColor: colors.white,
    borderRadius: 10,
    position: 'absolute',
    top: 12,
    left: 12,
    paddingHorizontal: 10,
    paddingVertical: 2,
  },
  chipLabel: { color: colors.black, fontSize: 16, fontWeight: 'bold' } as TextStyle,
  content: { marginTop: 10 },
  title: {
    fontWeight: 'bold',
    fontSize: 22,
    letterSpacing: 0.3,
  } as TextStyle,
  contentFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 5,
  },
  price: { fontSize: 18, fontWeight: '500' } as TextStyle,
});

export default React.memo(EnterpriseCard);
