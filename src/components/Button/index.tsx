import React from 'react';
import { Text, TouchableOpacity, StyleSheet, TextStyle } from 'react-native';
import colors from '../../assets/colors';

interface IButton {
  label: string;
  onPress: () => void;
  disabled?: boolean | undefined;
  style?: Object;
}

const Button = ({ label, onPress, disabled, style }: IButton) => {
  return (
    <TouchableOpacity
      style={[styles.button, disabled && { backgroundColor: colors.darkerGrey }, style]}
      disabled={disabled}
      onPress={onPress}>
      <Text style={styles.label}>{label}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    width: '100%',
    height: 55,
    backgroundColor: colors.Black80,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    fontSize: 18,
    letterSpacing: 0.5,
    color: colors.white,
    fontWeight: 'bold',
  } as TextStyle,
});

export default Button;
