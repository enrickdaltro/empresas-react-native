import React from 'react';
import { View, Image, StyleSheet, ActivityIndicator } from 'react-native';
import colors from '../../assets/colors';
import images from '../../assets/images';

const SplashScreen = () => {
  return (
    <View style={styles.container}>
      <Image source={images.logoSplash} style={styles.logo} />
      <ActivityIndicator size="large" color={colors.black} style={{ marginTop: 10 }} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
  },
  logo: {
    width: 150,
    height: 150,
    resizeMode: 'cover',
  },
});

export default SplashScreen;
