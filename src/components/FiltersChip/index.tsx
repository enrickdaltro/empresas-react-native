import React, { ReactElement } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, TextStyle } from 'react-native';
import colors from '../../assets/colors';
import { IFilterType } from '../../store/modules/enterprises/types';

interface IFilterChip {
  filters: IFilterType[] | null;
  onFilterPressed: (id: number) => void;
  isSelectedFilter: number;
}

const FiltersChip = ({ filters, onFilterPressed, isSelectedFilter }: IFilterChip) => {
  const renderChip = (item: IFilterType): ReactElement => {
    const isSelected = item.id === isSelectedFilter;
    return (
      <TouchableOpacity
        style={[styles.chip, isSelected && styles.selected]}
        onPress={() => onFilterPressed(item.id)}>
        <Text style={[styles.chipLabel, isSelected && styles.selectedLabel]}>
          {item.enterprise_type_name}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={filters}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => renderChip(item)}
        ListEmptyComponent={<View />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  chip: {
    marginRight: 10,
    borderWidth: 1,
    borderColor: colors.mediumGrey,
    borderRadius: 20,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  chipLabel: {
    color: colors.darkerGrey,
    fontSize: 15,
    letterSpacing: 0.4,
  } as TextStyle,
  selected: {
    backgroundColor: colors.grey,
    borderColor: colors.grey,
  },
  selectedLabel: { color: colors.Black80 } as TextStyle,
});

export default FiltersChip;
