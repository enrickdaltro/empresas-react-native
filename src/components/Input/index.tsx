import React, { Ref } from 'react';
import { TextInput, TextInputProps, StyleSheet } from 'react-native';
import colors from '../../assets/colors';

interface IIpnut extends TextInputProps {}

const Input = React.forwardRef(({ style, ...rest }: IIpnut, ref: Ref<TextInput>) => (
  <TextInput {...rest} style={[styles.input, style]} ref={ref} />
));

const styles = StyleSheet.create({
  input: {
    backgroundColor: colors.grey,
    width: '100%',
    height: 50,
    borderRadius: 10,
    color: colors.Black80,
    fontSize: 16,
    paddingHorizontal: 10,
  },
});

export default Input;
