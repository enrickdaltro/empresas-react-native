import { CommonActions, NavigationContainerRef } from '@react-navigation/native';
import * as React from 'react';

export const navigationRef = React.createRef<NavigationContainerRef | null>();

export function navigate(name: string, params?: object) {
  navigationRef.current?.navigate(name, params);
}

export function reset() {
  navigationRef.current?.dispatch(CommonActions.reset({ index: 0, routes: [{ name: 'Login' }] }));
}
