import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Login from '../screens/Login';
import Home from '../screens/Home/Index';
import Enterprise from '../screens/Enterprise';

import { RootStackParamList } from '../@types/navigation';
import { useDispatch, useSelector } from 'react-redux';
import { IApplicationState } from '../store/createStore';
import { loadApp } from '../store/modules/auth/actions';
import SplashScreen from '../components/Splash';

const Stack = createStackNavigator<RootStackParamList>();

const Router = () => {
  const { isSigned, isAppLoading } = useSelector((state: IApplicationState) => state.auth);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadApp());
  }, []);

  if (isAppLoading) {
    return <SplashScreen />;
  }

  return isSigned ? (
    <Stack.Navigator headerMode="none" initialRouteName="Home">
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Enterprise" component={Enterprise} />
    </Stack.Navigator>
  ) : (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Login" component={Login} />
    </Stack.Navigator>
  );
};

export default Router;
