import { RouteProp } from '@react-navigation/native';
import React, { ReactNode, useEffect } from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  SafeAreaView,
  StyleSheet,
  Image,
  TextStyle,
  ScrollView,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { RootStackParamList } from '../../@types/navigation';
import colors from '../../assets/colors';
import Header from '../../components/Header';
import { IApplicationState } from '../../store/createStore';
import {
  clearEnterpriseDetails,
  fetchingEnterpriseDetailsRequest,
} from '../../store/modules/enterprises/actions';
import { HEIGHT, iPhoneXLike } from '../../utils/dimensions';

interface IEnterpriseProp {
  route: RouteProp<RootStackParamList, 'Enterprise'>;
}

const Enterprise = ({ route }: IEnterpriseProp) => {
  /** Params */
  const enterpriseId = route.params.id;

  /** Global State */
  const { loadingDetails, enterprise } = useSelector(
    (state: IApplicationState) => state.enterprises,
  );

  /** Dispatch */
  const dispatch = useDispatch();

  /** Effects */
  useEffect(() => {
    dispatch(fetchingEnterpriseDetailsRequest(enterpriseId));

    return () => {
      dispatch(clearEnterpriseDetails());
    };
  }, []);

  /** Functions */
  const getLocalizedPrice = (price: number | undefined): string | undefined => {
    return price?.toLocaleString('en-GB', { style: 'currency', currency: 'GBP' });
  };

  const renderDetail = (): ReactNode => {
    if (enterprise) {
      return (
        <View>
          <Image
            source={{ uri: `https://empresas.ioasys.com.br${enterprise?.photo}` }}
            style={styles.image}
          />
          <View style={styles.sectionRow}>
            <Text style={styles.title}>{enterprise?.enterprise_name}</Text>
            <View style={styles.chip}>
              <Text style={styles.chipLabel}>
                {enterprise?.enterprise_type.enterprise_type_name}
              </Text>
            </View>
          </View>
          <View style={styles.sectionRow}>
            <Text style={styles.price}>{getLocalizedPrice(enterprise?.share_price)}</Text>
            <Text style={styles.price}>{`${enterprise?.city} / ${enterprise?.country}`}</Text>
          </View>
          <Text style={styles.descriptionTitle}>About the company</Text>
          <Text style={styles.description}>{enterprise?.description}</Text>
        </View>
      );
    }
  };

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.container}>
        <Header />
        <ScrollView showsVerticalScrollIndicator={false}>
          {loadingDetails && (
            <View style={styles.loaderView}>
              <ActivityIndicator size="large" color={colors.black} />
            </View>
          )}
          {!loadingDetails && <>{renderDetail()}</>}
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  loaderView: { flex: 1, justifyContent: 'center', alignItems: 'center' },
  image: { width: '100%', height: HEIGHT * 0.3, resizeMode: 'cover', marginBottom: 10 },
  sectionRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  title: { fontSize: iPhoneXLike ? 26 : 22, fontWeight: 'bold' } as TextStyle,
  price: { fontSize: 18, fontWeight: '500', color: colors.Black80 } as TextStyle,
  descriptionTitle: {
    marginTop: 20,
    paddingHorizontal: 10,
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 0.4,
  },
  description: {
    marginTop: 10,
    paddingHorizontal: 10,
    marginBottom: 10,
    fontSize: 18,
    textAlign: 'justify',
    color: colors.Black80,
  },
  chip: {
    borderWidth: 1,
    borderColor: colors.grey,
    borderRadius: 20,
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: colors.grey,
  },
  chipLabel: { fontSize: 16, color: colors.Black80, fontWeight: 'bold' } as TextStyle,
});

export default Enterprise;
