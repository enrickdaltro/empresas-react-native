import React, { useCallback, useRef, useState } from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Image,
  KeyboardAvoidingView,
  Platform,
  Alert,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import colors from '../../assets/colors';
import images from '../../assets/images';
import Button from '../../components/Button';
import Input from '../../components/Input';
import { IApplicationState } from '../../store/createStore';
import { signInRequest } from '../../store/modules/auth/actions';

const Login = () => {
  const passwordRef = useRef(null);

  /** Local State */
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  /** Global State */
  const { loading } = useSelector((state: IApplicationState) => state.auth);
  const dispatch = useDispatch();

  /** Functions */
  const isBtnDisabled = useCallback((): boolean => {
    if (email && password.length >= 8) {
      return false;
    }
    return true;
  }, [email, password]);

  const handleSubmit = useCallback(() => {
    const EMAIL_REGEX = /(.+)@(.+){2,}\.(.+){2,}/;
    const isEmailValid = EMAIL_REGEX.test(email);

    if (!isEmailValid || password.length < 8) {
      return Alert.alert('Hang on!', 'Something went wrong, please try again.');
    }

    const payload = {
      email,
      password,
    };

    dispatch(signInRequest(payload));
  }, [dispatch, email, password]);

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.container}>
        <SafeAreaView style={styles.screen}>
          <Image source={images.logo} style={styles.logo} />
          <Input
            placeholder="E-mail"
            value={email}
            onChangeText={(text) => setEmail(text)}
            autoCapitalize="none"
            autoCorrect={false}
            returnKeyType="next"
            textContentType="emailAddress"
            onSubmitEditing={() => passwordRef.current.focus()}
          />
          <Input
            placeholder="Password"
            style={{ marginTop: 15 }}
            ref={passwordRef}
            textContentType="password"
            secureTextEntry={true}
            value={password}
            onChangeText={(text) => setPassword(text)}
            onSubmitEditing={handleSubmit}
          />
          <Button
            label={loading ? 'Carregando...' : 'Entrar'}
            style={{ marginTop: 15 }}
            disabled={isBtnDisabled() || loading}
            onPress={handleSubmit}
          />
        </SafeAreaView>
      </KeyboardAvoidingView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: colors.white },
  screen: { justifyContent: 'center', alignItems: 'center', flex: 1, marginHorizontal: 20 },
  logo: { height: 100, width: 200, resizeMode: 'contain' },
});

export default Login;
