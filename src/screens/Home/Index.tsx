import { useNavigation } from '@react-navigation/native';
import React, { ReactNode, useCallback, useEffect, useState } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TextStyle,
  FlatList,
  ActivityIndicator,
  Image,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import colors from '../../assets/colors';
import images from '../../assets/images';
import EnterpriseCard from '../../components/EnterpriseCard';
import FiltersChip from '../../components/FiltersChip';
import Input from '../../components/Input';
import { IApplicationState } from '../../store/createStore';
import { signOut } from '../../store/modules/auth/actions';
import {
  fetchEnterprisesRequest,
  filterEnterprisesRequest,
} from '../../store/modules/enterprises/actions';
import { IFilterSearch } from '../../store/modules/enterprises/types';

interface IHomeState {
  name: string;
  enterprise_types: number | null;
}

const Home = () => {
  /** Local State */
  const [state, setState] = useState<IHomeState>({ name: '', enterprise_types: null });
  const [selectedFilter, setSelectedFilter] = useState<number>(0);

  /** Global State */
  const { loading, enterprises, filters, filtering } = useSelector(
    (state: IApplicationState) => state.enterprises,
  );
  const { user } = useSelector((state: IApplicationState) => state.auth);
  /** Dispatch */
  const dispatch = useDispatch();

  /** Effects */
  const navigation = useNavigation();

  useEffect(() => {
    dispatch(fetchEnterprisesRequest());
  }, []);

  /** Functions */
  const logout = () => {
    dispatch(signOut());
  };

  const handleFilterSearch = useCallback(
    (text: string) => {
      setState({ ...state, name: text });
      let payload: IFilterSearch = {};
      if (selectedFilter === 0) {
        payload = { name: text };
      } else {
        payload = { name: text, enterprise_types: selectedFilter };
      }
      dispatch(filterEnterprisesRequest(payload));
    },
    [state, selectedFilter, dispatch],
  );

  const handleSelectFilterType = useCallback(
    (id: number) => {
      if (selectedFilter === id) {
        return;
      }
      setSelectedFilter(id);
      setState({ ...state, enterprise_types: id });
      dispatch(filterEnterprisesRequest({ name: state.name, enterprise_types: id }));
    },
    [selectedFilter, state, dispatch],
  );

  const handlePressedCard = useCallback((id: number) => {
    navigation.navigate('Enterprise', { id });
  }, []);

  const renderList = (): ReactNode => {
    if (filtering) {
      return (
        <View style={{ marginTop: 30 }}>
          <ActivityIndicator size="large" style={{ marginTop: 10 }} color={colors.black} />
        </View>
      );
    } else {
      return (
        <View style={{ marginTop: 20, marginBottom: 120 }}>
          <FlatList
            data={enterprises}
            keyExtractor={(item) => `${item.enterprise_name} + ${item.id}`}
            initialNumToRender={10}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <EnterpriseCard item={item} onCardPressed={(id) => handlePressedCard(id)} />
            )}
            ListEmptyComponent={
              <View style={styles.emptyList}>
                <Text style={styles.emptyListText}>Sorry! We couldn't find any company.</Text>
              </View>
            }
          />
        </View>
      );
    }
  };

  return (
    <View style={styles.container}>
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.header}>
          <Text style={styles.title}>{`Hello, ${user?.name}`}</Text>
          <TouchableOpacity onPress={() => logout()}>
            <Image source={images.logout} style={{ height: 25, width: 25 }} />
          </TouchableOpacity>
        </View>
        {loading && (
          <View style={{ marginTop: 30 }}>
            <ActivityIndicator size="large" style={{ marginTop: 10 }} color={colors.black} />
          </View>
        )}
        {!loading && (
          <>
            <Input
              placeholder="Search for an enterprise!"
              style={styles.filter}
              value={state.name}
              onChangeText={(text) => handleFilterSearch(text)}
            />
            <FiltersChip
              filters={filters}
              onFilterPressed={(id) => handleSelectFilterType(id)}
              isSelectedFilter={selectedFilter}
            />

            {renderList()}
          </>
        )}
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
  },
  title: {
    fontWeight: '600',
    fontSize: 28,
    letterSpacing: 0.5,
  } as TextStyle,

  filter: {
    height: 40,
    marginTop: 10,
  },
  sectionTitle: { fontSize: 22, fontWeight: 'bold', letterSpacing: 0.5 } as TextStyle,
  emptyList: {
    alignItems: 'center',
  },
  emptyListText: { fontSize: 20, letterSpacing: 0.5, textAlign: 'center' } as TextStyle,
});

export default Home;
