# Enterprises Mobile App

## Summary

- [Summary](#summary)
- [About the Project](#about-the-project)
- [Prerequisites](#prerequisites)
- [Made with](#made-with)
- [Installation](#installation)
- [Troubleshooting](#troubleshooting)

## About the Project

This is the result of the test for a React Native Developer position.

### Prerequisites

To run this project, you will need the following packages installed:

- [NodeJS v10.16 or higher](https://nodejs.org/en/)
- [Xcode](https://apps.apple.com/us/app/xcode/id497799835?mt=12) or [Android Studio](https:/P/developer.android.com/studio)
- [Cocoapods](https://cocoapods.org/)
- [Yarn](https://yarnpkg.com/)
- [Git](https://git-scm.com/)

## Made with

The project was built using TypeScript with [React Native CLI](https://reactnative.dev/)

There are some other dependencies worth mentioning:

- Redux
- Redux-Saga
- React Navigation v5
- Eslint
- Prettier

### About the dependencies

`react-native-async-storage/async-storage`: Helps the app save data on the user's device memory used on React Native Apps.

`react-native-community/masked-view`: Necessary to use React Navigation V5.

`react-navigation/native`: React Navigation V5 main core.

`react-navigation/stack`: React Navigation V5 stack core so we can create stack navigation.

`axios`: It helps to handle HTTP requests in a better way.

`intl`: Since React Native runs an older version of Javascript for Android, we need this parser to use newer features from Javascript. (in this case, the method toLocaleString())

`react-native-gesture-handler`: Necessary to use React Navigation V5.

`react-native-safe-area-context`: Necessary to use React Navigation V5.

`react-native-screens`: Necessary to use React Navigation V5.

`react-redux`: Helps us instantiate a Global State Management within the app.

`redux-saga`: Works as middleware on Redux flow, helping us deal with side effects on our app.

`typesafe-actions`: Helps us reduce verbosity and complexity with types on redux.

## Installation

Clone the repository, then:

go to the correct branch (if you're not there yet):

```bash
git checkout empresas
```

install the dependencies:

```bash
yarn
```

Install iOS pods (If you're going to run on a macOS):

```bash
cd ios
```

```bash
pod install
```

Go back to the root of the repo:

```bash
cd ..
```


Run the app:

```bash
yarn android
```

or

```bash
yarn ios
```

## Troubleshooting?

You can contact [me](https://www.linkedin.com/in/enrickdaltro/) or you can also check [React Native environment setup](https://reactnative.dev/docs/environment-setup) page.

Made with ❤️ by [Enrick Daltro](https://www.linkedin.com/in/enrickdaltro/) 🤙
